package id.im.example.onlinecontact;
import android.support.v7.widget.*;
import android.widget.*;
import android.view.*;

public class ContactViewHolder extends RecyclerView.ViewHolder
{
	TextView textId,textName, textEmail, textAddress, textGender, textMobilePhone, textOfficePhone, textHomePhone;
	
	ContactViewHolder(View v){
		super(v);
		textId=(TextView) v.findViewById(R.id.textViewId);
		textEmail=(TextView) v.findViewById(R.id.textViewEmail);
		textAddress=(TextView) v.findViewById(R.id.textViewAddress);
		textGender=(TextView) v.findViewById(R.id.textViewGender);
		textMobilePhone=(TextView) v.findViewById(R.id.textViewMobilePhone);
		textOfficePhone=(TextView) v.findViewById(R.id.textViewOfficePhone);
		textHomePhone=(TextView) v.findViewById(R.id.textViewHomePhone);
		textName=(TextView) v.findViewById(R.id.textViewName);
	}
}

//copas string response sekedar biar kebayang isi responsenya untuk membuat class javanya
/*
{
  "contacts": [
    {
      "id": "c200",
      "name": "Ravi Tamada",
      "email": "ravi@gmail.com",
      "address": "xx-xx-xxxx,x - street, x - country",
      "gender": "male",
      "phone": {
        "mobile": "+91 0000000000",
        "home": "00 000000",
        "office": "00 000000"
      }
    }
	
	*/
