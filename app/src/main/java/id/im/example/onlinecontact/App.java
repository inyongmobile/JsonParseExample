package id.im.example.onlinecontact;
import android.app.*;
import com.androidnetworking.*;

//kita butuh ini untuk inisialisasi Library android networking
// jangan lupa pake class ini di android manifest
//Tag "application"
//attribute "name"

public class App extends Application
{

	@Override
	public void onCreate()
	{
		// TODO: Implement this method
		super.onCreate();
		AndroidNetworking.initialize(this);
	}
	
	
}
