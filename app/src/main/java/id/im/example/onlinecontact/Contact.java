package id.im.example.onlinecontact;

public class Contact
{
	//pada string response ada json array yg isinya satu atau lbih json object
	//yg mana itu adalah list dari contact
	//maka kita buat class contact.java untuk mewakili json object contact
	//ada beberapa variable yg isinya string
	//dan satu variable berisi json object (phone)
	public String id, name, email, address,gender;
	//kita tadi udh bikin class Phone.java untuk mewakili jsonObject phone
	public Phone phone;
}

//copas string response sekedar biar kebayang isi responsenya untuk membuat class javanya
/*
{
  "contacts": [
    {
      "id": "c200",
      "name": "Ravi Tamada",
      "email": "ravi@gmail.com",
      "address": "xx-xx-xxxx,x - street, x - country",
      "gender": "male",
      "phone": {
        "mobile": "+91 0000000000",
        "home": "00 000000",
        "office": "00 000000"
      }
    }
	
	*/
