package id.im.example.onlinecontact;

import android.app.*;
import android.os.*;
import android.support.v7.widget.*;
import android.view.*;
import java.util.*;
import com.androidnetworking.*;
import com.androidnetworking.interfaces.*;
import com.androidnetworking.error.*;
import android.widget.*;
import android.content.*;
import android.preference.*;
import com.google.gson.*;

public class MainActivity extends Activity 
{
	//Tag untuk fast android networking
	//agar bisa cancel api saat ondestrroy
	private static final String TAG=MainActivity.class.getName();
	
	//String key untuk menyimpan string response ke sharesPreference
	//sehingga tetap bisa nampilin list contact
	//wlopun blm load ke server
	private static final String PREF_SAVE_KEY="contact_api_response";
	
	//list untuk menampung object contact
	private List<Contact> list;
	
	//adapter untuk recyclerview
	private ContactListAdapter adapter;
	
	//progressbar sebagai tanda sedang loading
	private View progressBar;
	
	//shared preferense untuk menyimpan response 
	//agar  bisa langsung kita tampilkan
	//untuk berikutnya
	//tanpa menunggu response dari server
	private SharedPreferences preferences;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		
		//Inisialisasi recyclerview
		RecyclerView rv=(RecyclerView) findViewById(R.id.recyclerView);
		rv.setLayoutManager(new LinearLayoutManager(this));
		//di sini kita tampilkan divider
		//untuk memisahkan setiap item contact
		rv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
		
		//setRecyclerview dgn list kontak yg masih kosong
		//kita akan isi listnya nanti begitu udh dapat reaponse dari rest api
		list=new ArrayList<>();
		adapter=new ContactListAdapter(list);
		rv.setAdapter(adapter);
		
		progressBar=findViewById(R.id.progressBar);
		preferences=PreferenceManager.getDefaultSharedPreferences(this);
		
		//Cek apakah ada response tersimpan
		//langsung tampilkan jika ada
		String saved=preferences.getString(PREF_SAVE_KEY, "");
		//pastikan string tersimpan diawali huruf {
		if(saved.startsWith("{")){
			ContactListApiResponse response=new Gson().fromJson(saved, ContactListApiResponse.class);
			updateList(response);
		}
		
		//load list dari server
		//tidak peduli udh/blm ada response tersimpan
		loadListDariServer();
    }
	
	private void loadListDariServer(){
		//tampilkan progressbar
		progressBar.setVisibility(View.VISIBLE);
		//kita akan load reaponse dalam bentuk string
		//agar bisa kita save ke sharedpreferense
		AndroidNetworking.get("https://api.androidhive.info/contacts/").setTag(TAG).build()
			.getAsString(new StringRequestListener(){

				@Override
				public void onResponse(String s)
				{
					progressBar.setVisibility(View.INVISIBLE);
					
					//cek responsenya bener berupa json atau tidak
					if(!s.startsWith("{")){
						Toast.makeText(MainActivity.this, "Error response bukan json", Toast.LENGTH_SHORT).show();
						return;
					}
					
					//response valid. simpan ke preferense
					preferences.edit().putString(PREF_SAVE_KEY, s).apply();
					//parse response
					ContactListApiResponse r=new Gson().fromJson(s, ContactListApiResponse.class);
					updateList(r);
					
				}

				@Override
				public void onError(ANError p1)
				{
					progressBar.setVisibility(View.INVISIBLE);
					Toast.makeText(MainActivity.this, p1.getErrorDetail(), Toast.LENGTH_SHORT).show();
				}
			});
		
	}
	
	
	private void updateList(ContactListApiResponse apiResponse){
		//hentikan proses jika api response null
		if( apiResponse==null) return;
		//kosongkan list
		list.clear();
		//isi ulang list dari response
		list.addAll(apiResponse.contacts);
		//notify adapter
		adapter.notifyDataSetChanged();
	}

	@Override
	protected void onDestroy()
	{
		AndroidNetworking.cancel(TAG);
		super.onDestroy();
	}
	
	
}
