package id.im.example.onlinecontact;

public class Phone
{
	//pada response api ada variable phone
	//yg berupa json object
	//maka kita bikin class javanya untuk merubah json object "phone"
	//menjadi java object
	//ada 3 variable pada jsonObjectnya,
	//dan isinya hanya string
	
	public String mobile, home, office;
}

//copas string response sekedar biar kebayang isi responsenya untuk membuat class javanya
/*
{
  "contacts": [
    {
      "id": "c200",
      "name": "Ravi Tamada",
      "email": "ravi@gmail.com",
      "address": "xx-xx-xxxx,x - street, x - country",
      "gender": "male",
      "phone": {
        "mobile": "+91 0000000000",
        "home": "00 000000",
        "office": "00 000000"
      }
    }
	
	*/
