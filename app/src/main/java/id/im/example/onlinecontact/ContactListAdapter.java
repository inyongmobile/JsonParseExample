package id.im.example.onlinecontact;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import java.util.*;

public class ContactListAdapter extends RecyclerView.Adapter<ContactViewHolder>
{
	private List<Contact> list;

	public ContactListAdapter(List<Contact> list)
	{
		this.list = list;
	}
	
	

	@Override
	public ContactViewHolder onCreateViewHolder(ViewGroup p, int p2)
	{
		return new ContactViewHolder(LayoutInflater.from(p.getContext()).inflate(R.layout.item_contact, p, false));
	}

	@Override
	public void onBindViewHolder(ContactViewHolder holder, int position)
	{
		Contact c=list.get(position);
		holder.textId.setText(c.id);
		holder.textName.setText(c.name);
		holder.textEmail.setText(c.email);
		holder.textAddress.setText(c.address);
		holder.textGender.setText(c.gender);
		holder.textMobilePhone.setText(c.phone.mobile);
		holder.textOfficePhone.setText(c.phone.office);
		holder.textHomePhone.setText(c.phone.home);
	}

	@Override
	public int getItemCount()
	{
		return list.size();
	}
	
}
