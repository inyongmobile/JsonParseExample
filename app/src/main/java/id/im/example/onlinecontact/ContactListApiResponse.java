package id.im.example.onlinecontact;
import java.util.*;

public class ContactListApiResponse
{
	//pada response apinya cuma ada satu variable
	//yaitu "contacts"
	//yg berisi jsonArray
	//karena berupa json array kita ubah menjadi List (ArrayList)
	
	public List<Contact> contacts =new ArrayList<>();
	
	
	//krena di string response cuma ada satu variable yaitu "contacts", 
	//berarti udh, cuma segini class ContactApiResponse_nya
}

//copas string response sekedar biar kebayang isi responsenya untuk membuat class javanya
/*
{
  "contacts": [
    {
      "id": "c200",
      "name": "Ravi Tamada",
      "email": "ravi@gmail.com",
      "address": "xx-xx-xxxx,x - street, x - country",
      "gender": "male",
      "phone": {
        "mobile": "+91 0000000000",
        "home": "00 000000",
        "office": "00 000000"
      }
    }
	
	*/
